#include <mdk.h>

int main(void) {
    for (;;) {
        // So compiler doesn't optimize out the loop
        __asm__ volatile("nop");
    }

    return 0;
}
