#!/usr/bin/env bash
set -eo pipefail

# In case previous instance still lingers
killall openocd || true

make build

# Use espressif's openocd for now
source ../../esp-idf/export.sh
openocd -f board/esp32c3-builtin.cfg -c "program_esp firmware.bin 0x10000 verify"
OPENOCD_PID=$!
_term() {
  echo "Caught signal!" 
  kill -TERM "$OPENOCD_PID"
  killall openocd
}
trap _term SIGTERM
trap _term SIGINT

#dir=$(pwd)
#echo "LAUNCHING GDB!"
#riscv32-unknown-elf-gdb -x .gdbinit ./firmware.elf