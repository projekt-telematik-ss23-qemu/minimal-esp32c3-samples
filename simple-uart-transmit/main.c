// All rights reserved

#include <mdk.h>
#include "esp32-c3-uart-interface.h"

int main(void) {
  wdt_disable();

  delay_ms(1000);
  init_uart();
  config_uart();
  enable_uart_transmitter();

  return 0;
}