target extended-remote :3333
set remote hardware-watchpoint-limit 2

add-symbol-file ../esp32c3_rev3_rom.elf
file ./firmware.elf

mon reset halt
flushregs