This repository contains small demo programs for the ESP32-C3, used to validate our qemu emulation.

They're built against [mdk](https://github.com/cpq/mdk), as this allows finer control
over what hardware features exactly are required, enabling gradual emulator development.

We have patched the SDK slightly. Our fork is [here](https://git.imp.fu-berlin.de/projekt-telematik-ss23-qemu/mdk).

## Getting started

After cloning the repo, run:

```sh
git submodule init
git submodule update --init --recursive
cd sample-you-want-to-run
make -j $(nproc)
```