#ifndef ESP32_C3_REGISTER_INTERFACE_H
#define ESP32_C3_REGISTER_INTERFACE_H

#include <stdint.h>

#define C3_UART_CONTROLLER_0_BASE ((uint32_t)0x60000000)
#define C3_UART_CONTROLLER_1_BASE ((uint32_t)0x60010000)
#define C3_UART_CONTROLLER_SELECT(base_select) ((uint32_t)((base_select == 1) ? C3_UART_CONTROLLER_1_BASE : C3_UART_CONTROLLER_0_BASE))

typedef volatile struct {
    uint32_t SCLK_DIV_B:    6;
    uint32_t SCLK_DIV_A:    6;  
    uint32_t SCLK_DIV_NUM:  8;
    uint32_t SCLK_SEL:      2;
    uint32_t SCLK_EN:       1; 
    uint32_t REST_CORE:     1;
    uint32_t TX_SCLK_EN:    1;
    uint32_t RX_SCLK_EN:    1;
    uint32_t TX_REST_CORE:  1;
    uint32_t RX_REST_CORE:  1;
    uint32_t RESERVED:      4;
} esp32c3_uart_clk_conf_t;

#define UART_CLK_CONF_REG(base_select) ((esp32c3_uart_clk_conf_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0078))  // Assuming the offset of clk_conf is 0x18 from the base address

void readSetClear();

#endif