#include "esp32-c3-register-interface.h"

void readSetClear(){
  esp32c3_uart_clk_conf_t *clockConfig = UART_CLK_CONF_REG(0);
  
  // set UART_RST_CORE
  clockConfig->REST_CORE = 1;
  
  // clear UART_RST_CORE
  clockConfig->REST_CORE = 0;
}