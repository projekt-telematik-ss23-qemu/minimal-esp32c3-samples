#include <mdk.h>
#include "esp32-c3-uart-interface.h"
#include "esp32-c3-led-helpers.h"

int main(void) {
  uint8_t green[3] = {1,0,0};
  uint8_t black[3] = {0,0,0};
  setupLED();
  showColor(black);
  // for some reason the first time we show a color, it's always a bright green...
  showColorForDuration(green,1000);

  init_uart();
  config_uart();

  showColor(black);

  return 0;
}
