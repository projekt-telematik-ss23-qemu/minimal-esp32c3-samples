#include "esp32-c3-led-helpers.h"
#include <mdk.h>

// used for debugging

// On the ESP32C3 dev boards, the WS2812 LED is connected to GPIO 8
static int ws_2812_pin = 8;

void setupLED(){
  wdt_disable();
  gpio_output(ws_2812_pin);
}

void blinkGreen(int count, unsigned long delay_millis) {
  uint8_t green[3] = {1, 0, 0}, black[3] = {0, 0, 0};
  for (int i = 0; i < count; i++) {
    ws2812_show(ws_2812_pin, green, sizeof(green));
    delay_ms(delay_millis);
    ws2812_show(ws_2812_pin, black, sizeof(black));
    delay_ms(delay_millis);
  }
}

void showColorForDuration(uint8_t grb[3], uint32_t durationInMs){
    ws2812_show(ws_2812_pin, grb, sizeof(uint8_t)*3);
    delay_ms(durationInMs);
}

void showColor(uint8_t grb[3]){
    ws2812_show(ws_2812_pin, grb, sizeof(uint8_t)*3);
}