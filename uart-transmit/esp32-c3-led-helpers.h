#ifndef ESP32_C3_LED_HELPERS_H
#define ESP32_C3_LED_HELPERS_H

void showColorForDuration();
void showColor();
void blinkGreen();
void setupLED();

#endif