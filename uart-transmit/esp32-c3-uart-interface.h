#ifndef ESP32_C3_UART_INTERFACE_H
#define ESP32_C3_UART_INTERFACE_H

// all addresses are relative to some other register....

#define C3_BASE_OFFSET_SYSTEM_REGISTERS ((uint32_t)0x600C0000)

#define C3_UART_CONTROLLER_0_BASE ((uint32_t)0x60000000)
#define C3_UART_CONTROLLER_1_BASE ((uint32_t)0x60010000)
#define C3_UART_CONTROLLER_SELECT(base_select) ((volatile uint32_t *)((base_select == 1) ? C3_UART_CONTROLLER_1_BASE : C3_UART_CONTROLLER_0_BASE))

#define C3_UART_FIFO_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0000))

#define C3_UART_INT_RAW(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0004))
// UART_TXFIFO_EMPTY_INT_RAW is on 1

#define C3_UART_INT_ENA_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x000C))
// UART_TXFIFO_EMPTY_INT_ENA is on 1

#define C3_UART_INT_CLR_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0010))
// UART_TXFIFO_EMPTY_INT_CLR

#define C3_UART_CLKDIV_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0014))
// UART_CLKDIV - integer part of baud rate calculator devisor - 0 to 11
// UART_CLKDIV_FRAG - numerator of devisor in baud rate calculator devisor

#define C3_UART_RX_FILT_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0018))
// SYSTEM_UARTn_RST n=0 is on 2, n=1 is on 5

#define C3_UART_CONF0_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0020))
// UART_PARITY is on 0
// UART_PARITY_EN is on 1
// UART_BIT_NUM is on 2 and 3

#define C3_UART_CONF1_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0024))
// UART_TXFIFO_EMPTY_THRHD is 9 to 17

#define C3_UART_CLK_CONF_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0078))
// UART_RST_CORE is on 23
// UART_SCLK_SEL is on 20 and 21 (2 bits), selcetion is between 1 and 3
// UART_SCLK_DIV_NUM - The integral part of the frequency divisor - 12 to 19
// UART_SCLK_DIV_A - The numerator of the frequency divisor - 6 to 11
// UART_SCLK_DIV_B - The denominator of the frequency divisor - 0 to 5

#define C3_UART_ID_REG(base_select) ((volatile uint32_t *)(C3_UART_CONTROLLER_SELECT((base_select)) + 0x0080))
// UART_UPDATE_CTRL is on 30
// UART_REG_UPDATE is on 31

#define C3_SYSTEM_PERIP_CLK_EN0_REG ((volatile uint32_t *)(C3_BASE_OFFSET_SYSTEM_REGISTERS+0x0010))
// SYSTEM_UART_MEM_CLK_EN is on 24
// SYSTEM_UARTn_CLK_EN n=0 is on 2, n=1 is on 5

void config_uart();
void init_uart();
void reset_uart();
void enable_uart_transmitter();
void writeSomething();

#endif